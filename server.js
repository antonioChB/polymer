var express = require('express'),
    app = express(),
    port = process.env.PORT || 3007;

var path = require('path');
var cors = require('cors');

app.use(cors())

app.use(express.static(__dirname + '/build/es6-unbundled'))

app.listen(port);

console.log('Proyecto Polymer con wrapper de NodeJS escuchando en: ' + port );

app.get('/', function (req, res) {
    res.sendfile('index.html', {root: '.'})
})
