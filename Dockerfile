# Imagen BAse

FROM node:latest

# Carpeta raiz contenedor

WORKDIR /app

#Copiado de archivos

ADD build/es6-unbundled /app/build/es6-unbundled
ADD package.json /app
ADD src/* /app/src/
ADD index.html /app
ADD polymer.json /app
ADD bower.json /app
ADD server.js /app
ADD service-worker.js /app
ADD sw-precache-config.js /app
ADD package-lock.json /app
ADD manifest.json /app

#Dependencias

RUN npm install
RUN npm install -g bower

# Puerto que exponemos
EXPOSE 3007

#Comandos para ejecutar la aplicacion
CMD ["npm","start"]
#CMD ["bower","install"]
#CMD ["node","server.js"]
